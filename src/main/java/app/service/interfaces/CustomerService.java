package app.service.interfaces;

import app.entity.Address;
import app.entity.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerService {
    Customer createCustomer(Customer customer);

    Customer updateCustomer(Customer customer);

    Customer findCustomerByID(UUID id);

    List<Customer> findCustomersByFirstNameAndLastName(String firstName, String lastName);

    List<Customer> findCustomersByAddress(Address address);

    Customer findCustomerByCardNumber(int cardNumber);

    List<Customer> findCustomerWithExpiredCards(int expirationDate);
}
