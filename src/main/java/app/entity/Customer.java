package app.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@EqualsAndHashCode(of = "id")
@Setter
@Getter
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @Id
    private UUID id;
    private String firstName;
    private String lastName;
    private Address address;
    private Account account;
}
