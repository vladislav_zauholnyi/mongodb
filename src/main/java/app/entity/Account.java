package app.entity;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder
@Getter
@Setter
@ToString
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private int cardNumber;
    private String nameOnAccount;
    private int expirationDate;
}
