package app.repository;

import app.entity.Address;
import app.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository extends MongoRepository<Customer, UUID> {
    List<Customer> findByFirstNameAndLastName(String firstName, String lastName);

    List<Customer> findByAddress(Address address);

    Optional<Customer> findByAccountCardNumber(int cardNumber);

    List<Customer> findByAccountExpirationDateLessThan(int expirationDate);
}
