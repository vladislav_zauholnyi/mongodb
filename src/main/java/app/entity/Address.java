package app.entity;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder
@Getter
@Setter
@ToString
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    private String city;
    private String street;
    private int countryCode;
}
