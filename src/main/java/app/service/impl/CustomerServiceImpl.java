package app.service.impl;

import app.entity.Address;
import app.entity.Customer;
import app.repository.CustomerRepository;
import app.service.interfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository repository;

    @Override
    public Customer createCustomer(Customer customer) {
        return repository.insert(customer);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        return repository.save(customer);
    }

    @Override
    public Customer findCustomerByID(UUID id) {
        Customer customer = null;
        Optional<Customer> optional = repository.findById(id);
        if (optional.isPresent()) {
            customer = optional.get();
        }
        return customer;
    }

    @Override
    public List<Customer> findCustomersByFirstNameAndLastName(String firstName, String lastName) {
        return repository.findByFirstNameAndLastName(firstName, lastName);
    }

    @Override
    public List<Customer> findCustomersByAddress(Address address) {
        return repository.findByAddress(address);
    }

    @Override
    public Customer findCustomerByCardNumber(int cardNumber) {
        Customer customer = null;
        Optional<Customer> optional = repository.findByAccountCardNumber(cardNumber);
        if (optional.isPresent()) {
            customer = optional.get();
        }
        return customer;
    }

    @Override
    public List<Customer> findCustomerWithExpiredCards(int expirationDate) {
        return repository.findByAccountExpirationDateLessThan(expirationDate);
    }
}
