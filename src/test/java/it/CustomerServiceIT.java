package it;

import app.entity.Account;
import app.entity.Address;
import app.entity.Customer;
import app.service.interfaces.CustomerService;
import config.TestConfig;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@SpringJUnitConfig(TestConfig.class)
public class CustomerServiceIT {

    @Autowired
    private CustomerService sut;

    @Test
    public void shouldCreateNewCustomer() {
        //GIVEN
        Customer customer = generateCustomer();

        //WHEN
        Customer customerAfterInsert = sut.createCustomer(customer);

        //THEN
        Assertions.assertEquals(customer, customerAfterInsert);
    }

    @Test
    public void shouldUpdateCustomer() {
        //GIVEN
        Customer initialCustomer = generateCustomer();
        sut.createCustomer(initialCustomer);

        //WHEN
        initialCustomer.setFirstName(RandomString.make());
        Customer customerAfterUpdate = sut.updateCustomer(initialCustomer);

        //THEN
        Assertions.assertEquals(initialCustomer, customerAfterUpdate);
    }

    @Test
    public void shouldFindCustomerById() {
        //GIVEN
        Customer customer = generateCustomer();
        sut.createCustomer(customer);

        //WHEN
        Customer customerById = sut.findCustomerByID(customer.getId());

        //THEN
        Assertions.assertEquals(customer, customerById);
    }

    @Test
    public void shouldFIndCustomersByFirstAndLastName() {
        //GIVEN
        Customer customer = generateCustomer();
        Customer customer2 = generateCustomer();
        Customer customer3 = generateCustomer();

        customer3.setFirstName(customer.getFirstName());
        customer3.setLastName(customer.getLastName());

        List<Customer> customers = List.of(customer, customer3);

        sut.createCustomer(customer);
        sut.createCustomer(customer2);
        sut.createCustomer(customer3);

        //WHEN
        List<Customer> customersFromDB = sut.findCustomersByFirstNameAndLastName(customer.getFirstName(), customer.getLastName());

        //THEN
        Assertions.assertEquals(customers, customersFromDB);
    }

    @Test
    public void shouldFindCustomerByAddress() {
        //GIVEN
        Customer customer = generateCustomer();
        Customer customer2 = generateCustomer();
        Customer customer3 = generateCustomer();

        customer3.setAddress(customer.getAddress());

        List<Customer> customers = List.of(customer, customer3);

        sut.createCustomer(customer);
        sut.createCustomer(customer2);
        sut.createCustomer(customer3);

        //WHEN
        List<Customer> customersFromDB = sut.findCustomersByAddress(customer.getAddress());

        //THEN
        Assertions.assertEquals(customers, customersFromDB);
    }

    @Test
    public void shouldFindCustomerByCardNumber() {
        //GIVEN
        Customer customer = generateCustomer();
        sut.createCustomer(customer);

        //WHEN
        Customer customerByCardNumber = sut.findCustomerByCardNumber(customer.getAccount().getCardNumber());

        //THEN
        Assertions.assertEquals(customer, customerByCardNumber);
    }

    @Test
    public void shouldFindCustomerWithCardNumberLessThanRequired() {
        //GIVEN
        Customer customer = generateCustomer();
        Account account = generateAccount();
        account.setExpirationDate(2021);
        customer.setAccount(account);
        sut.createCustomer(customer);

        Customer customer2 = generateCustomer();
        Account account2 = generateAccount();
        account2.setExpirationDate(2020);
        customer2.setAccount(account2);
        sut.createCustomer(customer2);

        Customer customer3 = generateCustomer();
        Account account3 = generateAccount();
        account3.setExpirationDate(2022);
        customer3.setAccount(account3);
        sut.createCustomer(customer3);

        List<Customer> customers = List.of(customer, customer2);

        //WHEN
        List<Customer> customersFromDB = sut.findCustomerWithExpiredCards(2022);

        //THEN
        Assertions.assertEquals(customers, customersFromDB);
    }

    private Customer generateCustomer() {
        Random random = new Random();
        Address address = Address.builder()
                .city(RandomString.make())
                .street(RandomString.make())
                .countryCode(random.nextInt(1000))
                .build();

        Account account = Account.builder()
                .cardNumber(random.nextInt(1000))
                .nameOnAccount(RandomString.make())
                .expirationDate(random.nextInt(1000))
                .build();

        return Customer.builder()
                .id(UUID.randomUUID())
                .firstName(RandomString.make())
                .lastName(RandomString.make())
                .address(address)
                .account(account)
                .build();
    }

    private Account generateAccount() {
        Random random = new Random();
        return Account.builder()
                .cardNumber(random.nextInt(1000))
                .nameOnAccount(RandomString.make())
                .expirationDate(random.nextInt(1000))
                .build();
    }
}
